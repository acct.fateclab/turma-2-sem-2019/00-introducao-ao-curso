# Informações sobre o Curso
Olá Aluno :)

Este curso possui o objetivo de contemplarmos os conceitos básciso das principais tecnologias usadas para o desenvolvimento web moderno.

Além disso iremos trabalhar com tecnologias open-source, a fim de inserção do aluno dentro desse universo.

Dentro dele, iremos cobrir as seguintes partes:
- React - Biblioteca para construção de Interfaces de Usuário (UI) criada pelo Facebook.
- React Native - Bibliotca para desenvolvimento de aplicativos nativos para Android e IOS.
- NodeJS - Ambiente de desenvolvimento backend para construções de APIs modernas usando a linguagem JavaScript.
- MongoDB - Banco de dados NOSQL escalável usado para persistência de dados.

## Intrutores

- Fabio: fabio@acct.global
- Gustavo: gustavo.vasconcellos@acct.global
- Gabriel: gabriel.carvalho@acct.global

## Slack
Disponibilizaremos um canal de comunicação online que poderá ser utilizado pelos alunos para tirar dúvidas em qualquer dia e horário da semana. A resposta será disponibiliza em até 1 dia útil.

Dentro do Slack teremos os canais por tema (#react, #reactnative, #nodejs, #mongodb) e o canal de anúncios gerais (#general).

Sempre que disponibilizarmos uma nova aula faremos o anúncio notificando no canal #general do Slack.

Link de Acesso: [https://join.slack.com/t/acctlab/shared_invite/enQtNTc1NzEyOTc0MDUwLWMzMzQ1MjAyMGQ2Zjg0YzVmNzY4YTEzNzYzMmJjY2Q2MzE5OWYwNzZkMTI0ZTQ1Nzc1MzFjNDhjNTc4YjEzY2I](https://join.slack.com/t/acctlab/shared_invite/enQtNTc1NzEyOTc0MDUwLWMzMzQ1MjAyMGQ2Zjg0YzVmNzY4YTEzNzYzMmJjY2Q2MzE5OWYwNzZkMTI0ZTQ1Nzc1MzFjNDhjNTc4YjEzY2I)

## Programação do Curso

### Carga horária:
- Duração total do curso de 4 meses - Haverão 2 turmas por ano.
- Total de dedicação mínima semanal: 5 horas semanais (20h mensais) sendo:
-- 2 horas presenciais semanais no laboratório FATEC - Horário 17 às 19h, terças-feiras (total 8 horas mensais).
-- 3 horas extra-classe semanais - Horário definido pelo aluno.


### Primeira fase (40 horas):

#### Construção de aplicações web front-end com React UI, com ênfase em Progressive Web Apps (Google). - 20 horas
- Semana 1 (27/08) - Tema: Introdução ao React e Configurações de Ambiente (Git, Gitlab, NPM, create-react-app) 
- Semana 2 (03/09) - Uso de Components and Props, State and Lifecycle, Handling Events,  Conditional Rendering.
- Semana 3 (10/09) - Lists and Keys, Forms, Lifting State Up, Composition vs Inheritance
- Semana 4 (17/09) - Conceitos de PWA em aplicações ReactJS.

#### Construções de aplicativos nativos com React Native. - 20 horas
- Semana 1 (24/09) - Getting Started, Learn the Basics.
- Semana 2 (0/10) - Props, State, Style, Height and Width, Layout with Flexbox.
- Semana 3 (08/10) - Handling Text Input, Handling Touches, Using a ScrollView, Using List Views
- Semana 4 (15/10) - Requisições e `fetch`, Networking

### Segunda fase (40 horas):

#### Construções de aplicações web back-end e API's com NODEJS. - 20 horas
- Semana 1 (22/10) - Introdução ao Node.js
- Semana 2 (29/10) - Criação de APIs com Express
- Semana 3 (05/11) - Migrando nossa API para Typescript 
- Semana 4 (12/11) - Headers e Cabeçalhos HTTP


### Persistência de dados em banco de dados NOSQL com MongoDB e desafio final. - 20  horas
- Semana 1 (19/11) - Introdução ao MongoDB
- Semana 2 (26/11) - Desenvolvimento Desafio Final
- Semana 3 (03/12) - Desenvolvimento Desafio Final
- Semana 4 (10/12) - Apresentação Desafio Final
